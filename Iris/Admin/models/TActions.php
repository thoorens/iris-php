<?php

namespace Iris\Admin\models;

/*
 * This file is part of IRIS-PHP.
 *
 * IRIS-PHP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * IRIS-PHP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IRIS-PHP.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @copyright 2012 Jacques THOORENS
 */

/**
 * Model for the current application
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ */
class TActions extends _IrisObject {

    protected static $_InsertionKeys = ['Name', 'controller_id'];

    public static function DDLText() {
        return <<<SQL2
CREATE  TABLE "main"."actions" (
        "id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 
        "Name" TEXT NOT NULL, 
        "controller_id" INTEGER NOT NULL,
        "Deleted" BOOLEAN DEFAULT 0,
        FOREIGN KEY ("controller_id") REFERENCES "controllers"("id"));
SQL2;
    }

}

